from functools import wraps
from operator import or_,and_
from myapp.common.database import database as db
from flask_restful import abort
import jwt
from werkzeug.security import check_password_hash, generate_password_hash

def check_if_already_exists(model,emailId,number):
    result=model.query.filter(or_(model.email==emailId, model.number==number)).all()
    # result = model.query.filter_by(email=emailId).first()
    if result:
        if result[0].email==emailId:
             abort(409, message={"email" :"id already registered"})
        if result[0].number==number:
             abort(409, message={"number" :"already registered"})

def check_login_info(model,emailId,password):
    result=model.query.filter(model.email==emailId).all()
    if result:
        if check_password_hash(result[0].password,password):
               return result
        else:
                abort(401, message={"user":"Unauthorized user"})
    else:
         abort(401, message={"email" :"id not registered"})

def get_user_details(model,emailId):
    result=model.query.filter(model.email==emailId).all()
    if result:
        return result
    else:
        abort(401, message={"email" :"id not registered"})


