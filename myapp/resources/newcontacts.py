from flask_restful import Resource, reqparse,fields,marshal_with
from operator import or_,and_
from myapp.common.generic import check_if_already_exists ,get_user_details,check_login_info
from myapp.common.database import database as db
from myapp.resources.user import UserModel,authentication
from sqlalchemy import func

user_put_args = reqparse.RequestParser()
user_put_args.add_argument("useremail", type=str, help="email id of user required", required=True)
user_put_args.add_argument("password", type=str, help="password of user required", required=True)

user_put_args.add_argument("name", type=str, help="Name of contact required", required=True)
user_put_args.add_argument("email", type=str, help="email of contact required")
user_put_args.add_argument("number", type=int, help="number of contact required", required=True)
user_put_args.add_argument("country", type=str, help="country of contact required")
user_put_args.add_argument("address", type=str, help="address of contact required")

user_get_details_args = reqparse.RequestParser()
user_get_details_args.add_argument("useremail", type=str, help="email id of user required", required=True)




class AddContactModel(db.Model):
    __tablename__="contacts"
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    userid=db.Column(db.BigInteger, db.ForeignKey(UserModel.id),nullable=True)
    name = db.Column(db.String(120), nullable=True)
    email = db.Column(db.String(120))
    number = db.Column(db.BigInteger, nullable=True)
    address= db.Column(db.String(200))
    country= db.Column(db.String(200))

    def __repr__(self) -> str:
        return super().__repr__()

resource_field={
    "name": fields.String,
    "email": fields.String,
    "number": fields.Integer,
    "country": fields.String,
    "address" : fields.String
}

class AddContact(Resource):
    @marshal_with(resource_field)
    @authentication
    def put(self):
        args= user_put_args.parse_args()
        isloged=check_login_info(UserModel,args['useremail'],args['password'])
        user = AddContactModel(name=args['name'], email=args['email'],number=args['number'],address=args['address'],country=args['country'],userid=isloged[0].id)
        db.session.add(user)
        db.session.commit()
        return user, 201


resource_user_details_field={
    "name": fields.String,
    "email": fields.String,
    "number": fields.Integer,
    "address" : fields.String
}

class UserDetails(Resource):
    @marshal_with(resource_user_details_field)
    @authentication
    def get(self):
        args= user_get_details_args.parse_args()
        result=get_user_details(UserModel,args['useremail'])
        return result, 201

resource_user_added_contact_details_field={
    "name": fields.String,
    "email": fields.String,
    "number": fields.Integer,
    "address" : fields.String,
    "country": fields.String
}
class UserAddedContactDetails(Resource):
    @marshal_with(resource_user_added_contact_details_field)
    @authentication
    def get(self):
        args= user_get_details_args.parse_args()
        result=get_user_details(UserModel,args['useremail'])
        user_added_contact=AddContactModel.query.filter(AddContactModel.userid==result[0].id).all()
        return user_added_contact, 201

class Search(Resource):
    @marshal_with(resource_user_added_contact_details_field)
    @authentication
    def get(self,search):
        args= user_get_details_args.parse_args()
        result=get_user_details(UserModel,args['useremail'])
        user_added_contact=AddContactModel.query.filter(and_(AddContactModel.userid==result[0].id,func.lower(AddContactModel.name)==func.lower(search))).all()
        if not user_added_contact:
            user_added_contact=AddContactModel.query.filter(and_(AddContactModel.userid==result[0].id,func.lower(AddContactModel.email)==func.lower(search))).all()
        if not user_added_contact:
             user_added_contact=AddContactModel.query.filter(and_(AddContactModel.userid==result[0].id,AddContactModel.number==search)).all()
        if not user_added_contact:
            user_added_contact={"message": "Not found"}
        # AddContactModel.query.filter(or_(and_(AddContactModel.userid==result[0].id,func.lower(AddContactModel.name)==func.lower(search)), and_(AddContactModel.userid==result[0].id,func.lower(AddContactModel.email)==func.lower(search)), and_(AddContactModel.userid==result[0].id,func.lower(AddContactModel.number)==func.lower(search)))).all()
        return user_added_contact, 201