
from flask_restful import Resource, reqparse,fields,marshal_with,abort
from werkzeug.security import generate_password_hash
from myapp.common.database import database as db
from myapp.common.generic import check_if_already_exists
from functools import wraps
from flask import request
from myapp.instance.config import config
import jwt
from werkzeug.security import generate_password_hash

user_put_args = reqparse.RequestParser()
user_put_args.add_argument("name", type=str, help="Name of user required", required=True)
user_put_args.add_argument("email", type=str, help="email of user required", required=True)
user_put_args.add_argument("number", type=int, help="number of user required")
user_put_args.add_argument("password", type=str, help="password of user required", required=True)
user_put_args.add_argument("address", type=str, help="address of user required")
resource_field={
    "name": fields.String,
    "email": fields.String,
    "number": fields.Integer,
    "password": fields.String,
    "address" : fields.String
}


class UserModel(db.Model):
    __tablename__="user"
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    name = db.Column(db.String(120), nullable=True)
    password = db.Column(db.String(200), nullable=True)
    email = db.Column(db.String(120), unique=True, nullable=True)
    number = db.Column(db.BigInteger, unique=True)
    address= db.Column(db.String(200))

    def __repr__(self) -> str:
        return super().__repr__()

def authentication(f):
    @wraps(f)
    def decorated(*args,**kwargs):
        token=None
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        if not token:
              abort(401, message={"token":"empty"})
        try:
            data= jwt.decode(token, config.SECRET_KEY ,algorithms='HS256')
            current_user=UserModel.query.filter(UserModel.email==data['public_id']).first()
            if not current_user:
                abort(401, message={"user":"Unauthorized user"})
        except Exception as e:
            abort(401, message={"user":"Unauthorized user"})
        return f(*args,**kwargs)
    return decorated

class User(Resource):
    @marshal_with(resource_field)
    def put(self):
        args= user_put_args.parse_args()
        user = UserModel(name=args['name'], email=args['email'],number=args['number'], password=generate_password_hash(args['password']),address=args['address'])
        check_if_already_exists(UserModel,args['email'],args['number'])
        db.session.add(user)
        db.session.commit()
        return user, 201


