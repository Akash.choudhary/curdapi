from flask import current_app, jsonify, make_response, request,abort
from flask_restful import Resource, reqparse,fields,marshal_with
from myapp.resources.user import UserModel,authentication
from myapp.common.generic import check_login_info
import jwt
import datetime
from functools import wraps


user_get_args = reqparse.RequestParser()
user_get_args.add_argument("email", type=str, help="email of user required", required=True)
user_get_args.add_argument("password", type=str, help="password of user required", required=True)
user_get_args.add_argument("token", type=str)

resource_field={
    "token": fields.String,
}

class Login(Resource):
    @marshal_with(resource_field)
    def get(self):
        args= user_get_args.parse_args()
        results = check_login_info(UserModel,args['email'],args['password'])
        args['token'] = jwt.encode({"public_id": results[0].email, "exp" : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, current_app.config['SECRET_KEY'])
        a =current_app.config['SECRET_KEY']
        return args,201

