from flask import Flask
from flask_restful import Api, abort
from myapp.resources.user import User
from myapp.resources.newcontacts import AddContact,UserDetails,UserAddedContactDetails,Search
from myapp.resources.login import Login
from myapp.common.database import database
from myapp.instance.config import config

def create_app():
    try:
        curdapp = Flask(__name__,instance_relative_config=True)
        curdapp.config.from_object(config)
        # curdapp.config.from_pyfile('config.py')
        database.init_app(curdapp)  
       
    except Exception as e:
        abort(503, messgae="Service unavailable")
   
    api = Api(curdapp)

    api.add_resource(User,"/signup/")
    api.add_resource(Login,"/login/")
    api.add_resource(UserDetails,"/userdetails/")
    api.add_resource(UserAddedContactDetails,"/useraddedcontactdetails/")
    api.add_resource(AddContact,"/addcontact/")
    api.add_resource(Search,"/search/<string:search>/")
    # database.create_all(app=curdapp)
    return curdapp



   